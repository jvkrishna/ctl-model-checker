import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class KripkeModel implements Model, Serializable {

	private static final long serialVersionUID = -453029749559897411L;
	private Map<Integer, State> statesMap;
	private boolean finish;
	private boolean trace;

	public KripkeModel(int debugLevel) {
		if (debugLevel > 0)
			trace = true;
		trace("Initialized Empty Model");
	}

	@Override
	public boolean finish() {
		trace("Checking Kripke structure.");
		for (State state : statesMap.values()) {
			if (!state.hasOutgoingEdges()) {
				print("State s%d has no outgoing edges. Invalid Kripke Model.", state.getId());
				return false;
			}
		}
		trace("Valid Kripke Model!.");
		finish = true;
		return true;
	}

	@Override
	public void setNumStates(int n) {
		trace("Initializing model states");
		this.statesMap = new HashMap<>();
		State s = null;
		for (int i = 0; i < n; i++) {
			s = new State(i);
			this.statesMap.put(i, s);
		}
		trace("Updated model with %d state objects", n);
	}

	@Override
	public boolean isValidState(int s) {
		return s >= 0 && s < statesMap.size();
	}

	@Override
	public void addArc(int s1, int s2) {
		if (finish) {
			print("Adding the model after model is finished");
			throw new RuntimeException("Model Finished");
		}
		//trace("Adding an arc from state %d to state %d", s1, s2);
		State si = statesMap.get(s1);
		State so = statesMap.get(s2);
		si.addOutEdge(so);
		so.addInEdge(si);
	}

	@Override
	public StateSet makeEmptySet() {
		return new KripkeStateSet();
	}

	@Override
	public void deleteSet(StateSet sset) {
		sset.clear();

	}

	@Override
	public void addState(int s, StateSet sset) {
		State state = statesMap.get(s);
		sset.add(state);
	}

	@Override
	public void copy(StateSet sset, StateSet rset) {
		rset.clear();
		rset.addAll(sset);

	}

	@Override
	public void NOT(StateSet sset, StateSet rset) {
		trace("taking complement states for %d states", sset.size());
		StateSet complementSet = new KripkeStateSet();
		for (State state : statesMap.values()) {
			if (!sset.contains(state)) {
				complementSet.add(state);
			}
		}
		copy(complementSet, rset);
		trace("Created a total of %d complement states", rset.size());
		assert (rset.size() + sset.size() == statesMap.size());

	}

	@Override
	public void EX(StateSet sset, StateSet rset) {
		trace("Calling EX on %d states", sset.size());
		StateSet exSet = new KripkeStateSet();
		for (State state : sset) {
			if (state.hasIncomingEdges()) {
				exSet.addAll(state.getInEdges());
			}
		}
		copy(exSet, rset);
		trace("Returned EX with %d states.", rset.size());
	}

	@Override
	public void EF(StateSet sset, StateSet rset) {
		trace("Calling EF on %d states", sset.size());
		StateSet allStates = new KripkeStateSet();
		allStates.addAll(statesMap.values());
		EU(allStates, sset, rset);
		trace("Returned EF with %d states.", rset.size());

	}

	@Override
	public void EG(StateSet sset, StateSet rset) {
		trace("Calling EG on %d states", sset.size());
		StateSet egSet = new KripkeStateSet();
		NOT(sset, egSet);
		AF(egSet, egSet);
		NOT(egSet, rset);
		trace("Returned EG with %d states.", rset.size());

	}

	@Override
	public void AX(StateSet sset, StateSet rset) {
		trace("Calling AX on %d states", sset.size());
		StateSet axSet = new KripkeStateSet();
		NOT(sset, axSet);
		EX(axSet, axSet);
		NOT(axSet, rset);
		trace("Returned AX with %d states.", rset.size());

	}

	@Override
	public void AF(StateSet sset, StateSet rset) {
		trace("Calling AF on %d states", sset.size());
		StateSet afSet = new KripkeStateSet();
		afSet.addAll(sset);
		boolean change = true;
		StateSet previousSet = null;
		while (change) {
			previousSet = previousLevel(afSet);
			if (previousSet.size() > 0) {
				afSet.addAll(previousSet);
			} else {
				change = false;
			}
			previousSet = null;
		}
		copy(afSet, rset);
		trace("Returned AF with %d states.", rset.size());

	}

	@Override
	public void AG(StateSet sset, StateSet rset) {
		trace("Calling AG on %d states", sset.size());
		StateSet agSet = new KripkeStateSet();
		NOT(sset, agSet);
		EF(agSet, agSet);
		NOT(agSet, rset);
		trace("Returned AG with %d states.", rset.size());

	}

	@Override
	public void AND(StateSet sset1, StateSet sset2, StateSet rset) {
		trace("Calling AND with %d, %d states.", sset1.size(), sset2.size());
		StateSet andSet = new KripkeStateSet();
		for (State state : sset1) {
			if (sset2.contains(state)) {
				andSet.add(state);
			}
		}
		copy(andSet, rset);
		trace("Returned AND with %d states.", rset.size());

	}

	@Override
	public void OR(StateSet sset1, StateSet sset2, StateSet rset) {
		trace("Calling OR with %d, %d states.", sset1.size(), sset2.size());
		StateSet orSet = new KripkeStateSet();
		orSet.addAll(sset1);
		orSet.addAll(sset2);
		copy(orSet, rset);
		trace("Returned OR with %d states.", rset.size());

	}

	@Override
	public void IMPLIES(StateSet sset1, StateSet sset2, StateSet rset) {
		trace("Calling IMPLIES with %d, %d states.", sset1.size(), sset2.size());
		StateSet impliesSet = new KripkeStateSet();
		NOT(sset1, impliesSet);
		OR(impliesSet, sset2, impliesSet);
		copy(impliesSet, rset);
		trace("Returned IMPLIES with %d states.", rset.size());

	}

	@Override
	public void EU(StateSet sset1, StateSet sset2, StateSet rset) {
		trace("Calling EU with %d, %d states.", sset1.size(), sset2.size());
		StateSet euSet = new KripkeStateSet();
		Queue<State> euQueue = new ArrayDeque<>();
		euQueue.addAll(sset2);
		euSet.addAll(sset2);
		State euState = null;
		while ((euState = euQueue.poll()) != null) {
			if (euState.hasIncomingEdges()) {
				for (State inEuState : euState.getInEdges()) {
					if (!euSet.contains(inEuState) && sset1.contains(inEuState)) {
						euQueue.add(inEuState);
						euSet.add(inEuState);
					}
				}
			}
		}
		copy(euSet, rset);
		trace("Returned EU with %d states.", rset.size());

	}

	@Override
	public void AU(StateSet sset1, StateSet sset2, StateSet rset) {
		trace("Calling AU with %d, %d states.", sset1.size(), sset2.size());
		StateSet auSet = new KripkeStateSet();
		StateSet nots1Set = new KripkeStateSet();
		StateSet nots2Set = new KripkeStateSet();
		StateSet euSet = new KripkeStateSet();
		StateSet t1Set = new KripkeStateSet();
		StateSet t2Set = new KripkeStateSet();
		StateSet egSet = new KripkeStateSet();
		NOT(sset1, nots1Set);
		NOT(sset2, nots2Set);
		AND(sset1, nots2Set, t1Set);
		AND(nots1Set, nots2Set, t2Set);
		EU(t1Set, t2Set, euSet);
		EG(nots2Set, egSet);
		OR(euSet, egSet, auSet);
		NOT(auSet, auSet);
		copy(auSet, rset);
		nots1Set = nots2Set = euSet = t1Set = t2Set = egSet = auSet = null;
		trace("Returned AU with %d states.", rset.size());

	}

	@Override
	public boolean elementOf(int s, StateSet sset) {
		return sset.contains(new State(s));
	}

	@Override
	public void display(StateSet sset) {
		List<State> stateList = new ArrayList<>(sset);
		Collections.sort(stateList);
		StringBuilder sb = new StringBuilder("{");
		for (State state : stateList) {
			sb.append('S').append(state.getId()).append(",");
		}
		String str = sb.toString();
		int i = str.lastIndexOf(",");
		if (i > 0) {
			str = str.substring(0, i);
		}
		print(str + "}");

	}

	/**
	 * Given set of states, find out states whose all outgoing edges are in
	 * input. It is analogous, in a reverse topological sort, find out states
	 * whose dependencies are given.
	 * 
	 * @param sset
	 *            Input set
	 * @param rset
	 *            Set of all states whose all outgoing edges are in sset.
	 */
	public StateSet previousLevel(StateSet sset) {
		StateSet rset = makeEmptySet();
		for (State state : sset) {
			if (state.hasIncomingEdges()) {
				for (State inState : state.getInEdges()) {
					if (!sset.contains(inState) && sset.containsAll(inState.getOutEdges())) {
						rset.add(inState);
					}
				}
			}
		}
		return rset;

	}

	private void trace(String s, Object... args) {
		if (trace)
			print(s, args);
	}

	private void print(String s, Object... args) {
		System.out.printf(s + "\n", args);
	}

}
