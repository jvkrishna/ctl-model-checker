import java.util.Collection;
import java.util.HashSet;

public class KripkeStateSet extends HashSet<State> implements StateSet {

	private static final long serialVersionUID = 1313810175190048542L;

	public KripkeStateSet() {
		super();
	}

	public KripkeStateSet(Collection<? extends State> coll) {
		super(coll);
	}

}
