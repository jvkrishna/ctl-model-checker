import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class State implements Serializable, Comparable<State> {
	private static final long serialVersionUID = -4507439771306004476L;

	private Integer id;
	private List<State> outEdges;
	private List<State> inEdges;

	public State(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<State> getOutEdges() {
		return outEdges;
	}

	public void setOutEdges(List<State> outEdges) {
		this.outEdges = outEdges;
	}

	public List<State> getInEdges() {
		return inEdges;
	}

	public void setInEdges(List<State> inEdges) {
		this.inEdges = inEdges;
	}

	/**
	 * Add an out edge from this state to given state.
	 * 
	 * @param s
	 */
	public void addOutEdge(State s) {
		if (this.outEdges == null) {
			this.outEdges = new LinkedList<>();
		}
		this.outEdges.add(s);
	}

	/**
	 * Add an incoming edge from given state to this state.
	 * 
	 * @param s
	 */
	public void addInEdge(State s) {
		if (this.inEdges == null) {
			this.inEdges = new LinkedList<>();
		}
		this.inEdges.add(s);
	}

	public boolean hasIncomingEdges() {
		return inEdges != null && inEdges.size() > 0;
	}

	public boolean hasOutgoingEdges() {
		return outEdges != null && outEdges.size() > 0;
	}

	@Override
	public String toString() {
		return this.id.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		return this.id == other.id;
	}

	@Override
	public int compareTo(State o) {
		return this.id.compareTo(o.id);
	}

}
